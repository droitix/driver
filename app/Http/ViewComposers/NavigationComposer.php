<?php

namespace openjobs\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class NavigationComposer
{
    public function compose(View $view)
    {
        if (!Auth::check()) {
            return $view;
        }

        $user = Auth::user();
        $listings = $user->listings;
        $resumes = $user->resumes;


        return $view->with([
            'unpublishedListingsCount' => $listings->where('live', false)->count(),
            'unpublishedResumesCount' => $resumes->where('live', false)->count(),
            'publishedResumesCount' => $resumes->where('live', true)->count(),

            'publishedListingsCount' => $listings->where('live', true)->count()
        ]);
    }
}
