<?php

namespace openjobs\Http\Controllers;

use Illuminate\Http\Request;
use openjobs\File;
use Auth;

class UploadController extends Controller
{


   public function index()
{

   $user=Auth::user();
   return view('user.files.upload.index', compact('user'));
}

 public function destroy(File $file)
    {
        $this->authorize('destroy', $file);

        $file->delete();

        notify()->success('Document Deleted!');

        return back()->withSuccess('Document was deleted.');
    }


public function upload(Request $request)
{
$this->validate($request, [
'cvs'=>'required',
]);
if($request->hasFile('cvs'))
{
$allowedfileExtension=['pdf','jpg','doc','png','docx'];
$files = $request->file('cvs');
foreach($files as $file){
$filename = $file->getClientOriginalName();
$extension = $file->getClientOriginalExtension();
$check=in_array($extension,$allowedfileExtension);
//dd($check);
if($check)
{

foreach ($request->cvs as $cv) {
$filename = $cv->store('vitae');
File::create([
'user_id' => auth()->user()->id,
'name' => $request->name,
'filename' => $filename
]);
}
notify()->success('Document Uploaded!');

  return redirect()
            ->route('files.published.index')
            ->withSuccess('Bid added.');

}
else
{
echo '<div class="alert alert-warning"><strong>Warning!</strong> Sorry Only Upload png , jpg , doc</div>';
}
}
}
}


}


