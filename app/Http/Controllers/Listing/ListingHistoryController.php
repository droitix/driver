<?php

namespace openjobs\Http\Controllers\Listing;

use Illuminate\Http\Request;
use openjobs\Http\Controllers\Controller;
use Auth;

class ListingHistoryController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth']);
    }

    public function index(Request $request)
    {

        $user=Auth::user();
        $listings = $request->user()->listings()->with(['area'])->latestFirst()->paginate(10);

        return view('user.listings.history.index', compact('listings','user'));
    }



}

