<?php

namespace openjobs\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReinvestCommentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'split' => 'min:2|gt:49',
            'body' => 'required | max:100',


        ];

    }
}
