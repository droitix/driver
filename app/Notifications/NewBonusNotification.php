<?php

namespace openjobs\Notifications;

use openjobs\Bonus;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NewBonusNotification extends Notification
{
    use Queueable;

    private $bonus;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Bonus $bonus)
    {
        $this->bonus = $bonus;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return array_merge($this->bonus->toArray(), [
            'notification_message' => 'You have recieved a bonus',
        ]);
    }
}
