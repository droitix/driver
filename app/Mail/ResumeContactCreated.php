<?php

namespace openjobs\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use openjobs\{User, Resume};

class ListingContactCreated extends Mailable
{
    use Queueable, SerializesModels;

    public $resume;

    public $sender;

    public $body;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Resume $resume, User $sender, $body)
    {
        $this->resume = $resume;
        $this->sender = $sender;
        $this->body = $body;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.resume.contact.message')
            ->subject("{$this->sender->name} sent a message about {$this->resume->title}")
            ->from('hello@fresh.com')
            ->replyTo($this->sender->email);
    }
}
