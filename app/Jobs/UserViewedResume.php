<?php

namespace openjobs\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use openjobs\{User, Resume};

class UserViewedResume implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $user;

    public $resume;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user, Resume $resume)
    {
        $this->user = $user;
        $this->resume = $resume;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $viewed = $this->user->viewedResumes;

        if ($viewed->contains($this->resume)) {
            $viewed->where('id', $this->resume->id)->first()->pivot->increment('count');
            return;
        }

        $this->user->viewedResumes()->attach($this->resume, [
            'count' => 1
        ]);
    }
}
