<?php

use Illuminate\Database\Seeder;

class AreaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $areas = [

            [

            'name' => 'South Africa',
                'children' => [
                    [
                        'name' => 'Rand',
                        'unit' => 'R',
                       
                       
                    ],
                    
                ],

           

            ],

               [

            'name' => 'Zambia',
                'children' => [
                    [
                        'name' => 'Kwacha',
                         'unit' => 'K',
                       
                       
                    ],
                    
                ],

           

            ],


               [

            'name' => 'Botswana',
                'children' => [
                    [
                        'name' => 'Pula',
                         'unit' => 'P',
                       
                       
                    ],
                    
                ],

           

            ],  


               [

            'name' => 'World',
                'children' => [
                    [
                        'name' => 'Dollar',
                         'unit' => '$',

                       
                       
                    ],
                    
                ],

           

            ], 

        

        ];

        foreach ($areas as $area) {
            \openjobs\Area::create($area);
        }
    }
}
