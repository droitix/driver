@extends('layouts.userapp')
@section('title')
   Edit Your Education | Openjobs360
@endsection
@section('content')
<!-- Our Dashbord -->
    <section class="our-dashbord dashbord">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-4 col-xl-3 dn-smd">
                    <div class="user_profile">
                        <div class="media">
                            <img src="/uploads/avatars/{{ Auth::user()->avatar }}" class="align-self-start mr-3 rounded-circle" alt="e1.png">

                            <div class="media-body">
                                <h5 class="mt-0">Hi, {{ Auth::user()->fullname }}</h5>
                                <p>{{ Auth::user()->province }}</p>
                            </div>

                        </div>
                    </div>
                    <div class="dashbord_nav_list">
                             <ul>

                            <li><a href="{{ route('profile') }}"><span class="flaticon-profile"></span> Profile</a></li>
                            <li class="active"><a href="{{ route('resumes.published.index', [$area]) }}"><span class="flaticon-resume"></span> Manage Resume</a></li>
                            <li><a href="#"><span class="flaticon-paper-plane"></span> Applied Jobs</a></li>
                            <li><a href="#"><span class="flaticon-analysis"></span> CV Manager</a></li>
                            <li><a href="{{ route('listings.favourites.index', [$area]) }}"><span class="flaticon-favorites"></span> Saved Jobs</a></li>
                            <li ><a href="{{ route('listings.viewed.index', [$area]) }}"><span class="flaticon-eye"></span> Jobs You Viewed</a></li>

                            <li><a  href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                            <span class="flaticon-logout"></span>{{ __('Logout') }}
                                        </a></li>

                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>


                        </ul>
                    </div>
                </div>
                <div class="col-sm-12 col-lg-8 col-xl-9">
                    <div class="my_profile_form_area">
                   <form action="{{ route('resumes.update', [$area, $resume]) }}" method="post">
                        <div class="row">
                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">Let recruiters know more about your education</h4>
                            </div>

                            <div class="col-lg-12 mt30">
                                <div class="my_profile_thumb_edit"></div>
                            </div>
                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Academic Institution</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" placeholder="UNISA" name="institution1" value="{{$resume->institution1}}">

                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">Name of Qualification</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2"  name="edutitle1" value="{{$resume->edutitle1}}">
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput7">Education Level</label><br>
                                    <select name="edulevel1" class="selectpicker">
                                        <option value="{{$resume->edulevel1}}">{{$resume->edulevel1}}</option>
                                        <option value="Certificate">Certificate</option>
                                        <option value="Matric">High School Matric</option>
                                        <option value="Tradee School">Trade School</option>
                                        <option value="Proffesional Qualification">Proffesional Qualification</option>
                                        <option  value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Masters">Masters</option>
                                        <option value="Doctorate">Doctorate</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="edufromyear1" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearstart')
                                 </select>
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                  <label for="exampleFormControlInput3">Finished</label><br>
                        <select name="edutoyear1" id="position" class="selectpicker">
                             @include('resumes.partials.forms._yearfinish')
                               </select>
                                </div>
                            </div>


                         <div class="col-lg-12">
                                <h4 class="fz20 mb20">Add another education(optional)</h4>
                            </div>


                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Academic Institution</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="institution2" value="{{$resume->institution2}}">

                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">Name of Qualification</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2"  name="edutitle2" value="{{$resume->edutitle2}}">
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput7">Education Level</label><br>
                                    <select name="edulevel2" class="selectpicker">
                                        <option value="{{$resume->edulevel2}}">{{$resume->edulevel2}}</option>
                                        <option value="Certificate">Certificate</option>
                                        <option value="Matric">High School Matric</option>
                                        <option value="Tradee School">Trade School</option>
                                        <option value="Proffesional Qualification">Proffesional Qualification</option>
                                        <option  value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Masters">Masters</option>
                                        <option value="Doctorate">Doctorate</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="edufromyear2" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearstart2')
                                 </select>
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                  <label for="exampleFormControlInput3">Finished</label><br>
                        <select name="edutoyear2" id="position" class="selectpicker">
                             @include('resumes.partials.forms._yearfinish2')
                               </select>
                                </div>
                            </div>

                         <div class="col-lg-12">
                                <h4 class="fz20 mb20">Maybe another Education will do(optional)</h4>
                            </div>

                              <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Academic Institution</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="institution3" value="{{$resume->institution3}}">

                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">Name of Qualification</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2"  name="edutitle3" value="{{$resume->edutitle3}}">
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput7">Education Level</label><br>
                                    <select name="edulevel3" class="selectpicker">
                                        <option value="{{$resume->edulevel3}}">{{$resume->edulevel3}}</option>
                                        <option value="Certificate">Certificate</option>
                                        <option value="Matric">High School Matric</option>
                                        <option value="Tradee School">Trade School</option>
                                        <option value="Proffesional Qualification">Proffesional Qualification</option>
                                        <option  value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Masters">Masters</option>
                                        <option value="Doctorate">Doctorate</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="edufromyear3" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearstart3')
                                 </select>
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                  <label for="exampleFormControlInput3">Finished</label><br>
                        <select name="edutoyear3" id="position" class="selectpicker">
                             @include('resumes.partials.forms._yearfinish3')
                               </select>
                                </div>
                            </div>

                            <div class="col-lg-12">
                                <h4 class="fz20 mb20">OK! make it fancy with another education(optional)</h4>
                            </div>

                              <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput1">Academic Institution</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput1" name="institution4" value="{{$resume->institution4}}">

                                </div>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <div class="my_profile_input form-group">
                                    <label for="formGroupExampleInput2">Name of Qualification</label>
                                    <input type="text" class="form-control" id="formGroupExampleInput2"  name="edutitle4" value="{{$resume->edutitle4}}">
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-6">
                                <div class="my_profile_select_box form-group">
                                    <label for="exampleFormControlInput7">Education Level</label><br>
                                    <select name="edulevel4" class="selectpicker">
                                        <option value="{{$resume->edulevel4}}">{{$resume->edulevel4}}</option>
                                        <option value="Certificate">Certificate</option>
                                        <option value="Matric">High School Matric</option>
                                        <option value="Tradee School">Trade School</option>
                                        <option value="Proffesional Qualification">Proffesional Qualification</option>
                                        <option  value="Diploma">Diploma</option>
                                        <option value="Degree">Degree</option>
                                        <option value="Masters">Masters</option>
                                        <option value="Doctorate">Doctorate</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">

                        <label for="exampleFormControlInput3">Start Year</label><br>
                         <select name="edufromyear4" id="position" class="selectpicker">

                             @include('resumes.partials.forms._yearstart4')
                                 </select>
                                </div>
                            </div>

                             <div class="col-md-6 col-lg-3">
                                <div class="my_profile_select_box form-group">
                                  <label for="exampleFormControlInput3">Finished</label><br>
                        <select name="edutoyear4" id="position" class="selectpicker">
                             @include('resumes.partials.forms._yearfinish4')
                               </select>
                                </div>
                            </div>





                             <input type="hidden" name="area_id" value="50">



                             <input type="hidden" id="custId" name="comp1" value="{{$resume->comp1}}">
                             <input type="hidden" id="custId" name="comp2" value="{{$resume->comp2}}">
                             <input type="hidden" id="custId" name="comp3" value="{{$resume->comp3}}">
                             <input type="hidden" id="custId" name="comp4" value="{{$resume->comp4}}">
                             <input type="hidden" id="custId" name="comp1title" value="{{$resume->comp1title }}">
                             <input type="hidden" id="custId" name="comp2title" value="{{$resume->comp2title }}">
                             <input type="hidden" id="custId" name="comp3title" value="{{$resume->comp3title }}">
                             <input type="hidden" id="custId" name="comp4title" value="{{$resume->comp4title }}">
                             <input type="hidden" id="custId" name="comp1loc" value="{{$resume->comp1loc}}">
                             <input type="hidden" id="custId" name="comp2loc" value="{{$resume->comp2loc}}">
                             <input type="hidden" id="custId" name="comp3loc" value="{{$resume->comp3loc}}">
                             <input type="hidden" id="custId" name="comp4loc" value="{{$resume->comp4loc}}">
                              <input type="hidden" id="custId" name="compfromyear1" value="{{$resume->compfromyear1}}">
                               <input type="hidden" id="custId" name="compfromyear2" value="{{$resume->compfromyear2}}">
                            <input type="hidden" id="custId" name="compfromyear3" value="{{$resume->compfromyear3}}">
                            <input type="hidden" id="custId" name="compfromyear4" value="{{$resume->compfromyear4}}">
                             <input type="hidden" id="custId" name="comptoyear1" value="{{$resume->comptoyear1  }}">
                             <input type="hidden" id="custId" name="comptoyear2" value="{{$resume->comptoyear2  }}">
                             <input type="hidden" id="custId" name="comptoyear3" value="{{$resume->comptoyear3  }}">
                             <input type="hidden" id="custId" name="comptoyear4" value="{{$resume->comptoyear4  }}">
                             <input type="hidden" id="custId" name="nxtjobtitle" value="{{$resume->nxtjobtitle}}">
                              <input type="hidden" id="custId" name="nxtjobrelocate" value="{{$resume->nxtjobrelocate}}">
                               <input type="hidden" id="custId" name="nxtjobtype" value="{{$resume->nxtjobtype}}">
                              <input type="hidden" id="custId" name="category_id" value="{{$resume->category_id}}">





                            <div class="col-lg-4">
                                <div class="my_profile_input">
                                    <button class="btn btn-lg btn-thm" type="submit">Save Education</button>
                                </div>
                            </div>

                             {{ csrf_field() }}
                             @method('PATCH')
                        </form><!-- form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@endsection

