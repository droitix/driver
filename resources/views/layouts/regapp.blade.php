<!DOCTYPE html>
<html lang="en">
<head>

   @include('layouts.partials.reghead')

</head>
   <body class="nk-body bg-white npc-default has-aside no-touch nk-nio-theme dark-mode as-mobile" theme="dark">

  

          <div id="app">

  <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                   <!-- content @s -->
                <div class="nk-content ">




       @yield('content')


        
       

       

@include('layouts.partials.footer')


                  </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->



    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js?ver=2.6.0"></script>
    <script src="./assets/js/scripts.js?ver=2.6.0"></script>
    <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/617b3c79f7c0440a59207ebd/1fj4m8fms';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>


</html>
