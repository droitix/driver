<!-- main header @s -->
                <div class="nk-header nk-header-fluid nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="{{url('dashboard')}}" class="logo-link">
                                    <img class="logo-light logo-img" src="/landing/assets/images/phone.png" srcset="/landing/assets/images/logo.png" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="/landing/assets/images/logo.png 2x" alt="logo-dark">
                                    <span class="nio-version"></span>
                                </a>
                              
                            </div>
                            
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                
                                                <div class="user-info d-none d-md-block">
                                                   
                                                    <div class="user-name dropdown-indicator">{{Auth::user()->name}} {{Auth::user()->surname}}</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                
                                            </div>
                                            <div class="dropdown-inner user-account-info">

               

                                               
                                                <div class="user-balance-sub"></div>
                                                
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                   
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="{{ route('logout') }}" class="nav-link"
                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();"><em class="icon ni ni-signout"></em><span>Sign out</span></a>
                                                       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                                     </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                   
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- main header @e -->