       <!-- sidebar @s -->
            <div class="nk-sidebar nk-sidebar-fixed " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-sidebar-brand">
                        <a href="{{url('dashboard')}}" class="logo-link nk-sidebar-logo">
                            <img class="logo-light logo-img" src="/landing/assets/images/logo.png" srcset="/landing/assets/images/logo.png 2x" alt="logo">
                            <img class="logo-dark logo-img" src="/landing/assets/images/logo.png" srcset="/landing/assets/images/logo.png 2x" alt="logo-dark">
                            <span class="nio-version"></span>
                        </a>
                    </div>
                    <div class="nk-menu-trigger mr-n2">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element">
                    <div class="nk-sidebar-body" data-simplebar>
                        <div class="nk-sidebar-content">
                            <div class="nk-sidebar-widget d-none d-xl-block">
                              
                            </div><!-- .nk-sidebar-widget -->
                            <div class="nk-sidebar-widget nk-sidebar-widget-full d-xl-none pt-0">
                               
                                <div class="nk-profile-content toggle-expand-content" data-content="sidebarProfile">
                                    <div class="user-account-info between-center">
                                        <div class="user-account-main">
                                                            @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)
                                        @php($multiplier = ($listing->amount-$listing->current))
 

                            @php ($sum += (($multiplier)*($percentage*$days)))

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                                            <h6 class="overline-title-alt">Account Balance</h6>
                                            <div class="user-balance">{{ Auth::user()->area->unit }} {{$sum-$diff}}.00 <small class="currency currency-btc"></small></div>
                                            
                                        </div>
                                        <a href="#" class="btn btn-icon btn-light"><em class="icon ni ni-line-chart"></em></a>
                                    </div>
                                    <ul class="user-account-data">
                                       
                                    </ul>
                                   
                                    
                                </div>
                            </div><!-- .nk-sidebar-widget -->
                            <div class="nk-sidebar-menu">
                                <!-- Menu -->
                                <ul class="nk-menu">
                                    <li class="nk-menu-heading">
                                        <h6 class="overline-title">Menu</h6>
                                    </li>
                                    <li><a href="{{url('dashboard')}}" class="btn btn-danger"><span>Dashboard</span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>


                                   <li><a href="{{ route('listings.published.index', [$area]) }}" class="btn btn-success"><span>Your Account</span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>

                                    <li><a href="{{ route('listings.create', [$area]) }}" class="btn btn-warning"><span>Buy Tokens</span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>
                                    
                                    <li><a href="{{ route('listings.published.index', [$area]) }}" class="btn btn-success"><span>Withdrawal</span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>

                                     <li><a href="{{url('profile')}}" class="btn btn-danger"><span>Profile Details </span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>

                                   <li><a href="{{url('referrals')}}" class="btn btn-danger"><span>Downliners </span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>
                                    <li><a href="{{ route('listings.bcreate', [$area]) }}" class="btn btn-danger"><span>Withdraw Bonus</span> <em class="icon ni ni-arrow-long-right"></em></a></li><br>
                                   
                                    
                                    
                                   
                                    @if (session()->has('impersonate'))
                                     <li class="nk-menu-item">
                                        <a onclick="event.preventDefault(); document.getElementById('impersonating').submit();" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-stop"></em></span>
                                            <span class="nk-menu-text">Stop Impersonating</span>
                                        </a>
                                               <form action="{{ route('admin.impersonate') }}" class="hidden" method="POST" id="impersonating">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                        </form>
                                    </li>
                                    @endif
                                    @role('admin')
                                    <li class="nk-menu-heading">
                                        <h6 class="overline-title">Admin Pages</h6>
                                    </li>
                                    <li class="nk-menu-item">
                                        <a href="{{ url('admin/impersonate')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Impersonate</span>
                                        </a>
                                    </li>
                                     <li class="nk-menu-item">
                                        <a href="{{ url('admin/listings')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Deposits</span>
                                        </a>
                                    </li>
                                        <li class="nk-menu-item">
                                        <a href="{{ url('admin/withdrawals')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Withdrawals</span>
                                        </a>
                                    </li>
                                     <li class="nk-menu-item">
                                        <a href="{{ url('admin/investments')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Investments</span>
                                        </a>
                                    </li>
                                      <li class="nk-menu-item">
                                        <a href="{{ url('admin/bonus')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Bonus Withdrawal</span>
                                        </a>
                                    </li>
                                        <li class="nk-menu-item">
                                        <a href="{{ url('admin/users')}}" class="nk-menu-link">
                                            <span class="nk-menu-icon"><em class="icon ni ni-lock"></em></span>
                                            <span class="nk-menu-text">Users</span>
                                        </a>
                                    </li>
                                    
                                    @endrole
                                </ul><!-- .nk-menu -->
                            </div><!-- .nk-sidebar-menu -->
                          
                          
                        </div><!-- .nk-sidebar-content -->
                    </div><!-- .nk-sidebar-body -->
                </div><!-- .nk-sidebar-element -->
            </div>
            <!-- sidebar @e -->