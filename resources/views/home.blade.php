<!DOCTYPE html>
<html lang="zxx">

<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/landing/assets/css/animate.min.css">
<link rel="stylesheet" href="/landing/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/landing/assets/css/fontawsome.min.css">
<link rel="stylesheet" href="/landing/assets/fonts/font/flaticon.css">
<link rel="stylesheet" href="/landing/assets/css/meanmenu.min.css">
<link rel="stylesheet" href="/landing/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/landing/assets/css/nice-select.min.css">
<link rel="stylesheet" href="/landing/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/landing/assets/css/magnific-popup.min.css">
<link rel="stylesheet" href="/landing/assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="/landing/assets/css/odometer.min.css">
<link rel="stylesheet" href="/landing/assets/css/barfiller.css">
<link rel="stylesheet" href="/landing/assets/css/style.css">
<link rel="stylesheet" href="/landing/assets/css/responsive.css">
<title>Quick Cash | Home</title>
<link rel="icon" type="image/png" href="/landing/assets/images/favicon.png">
</head>
<body>

<div class="header-area header-area-2">
<div class="navbar-area">

<div class="main-responsive-nav">
<div class="container">
<div class="mobile-nav">
<a href="{{url('/dashboard')}}" class="logo"><img src="/landing/assets/images/logo.png" alt="logo" /></a>
<ul class="menu-sidebar menu-small-device">
    @guest
<li><a class="default-button" href="{{url('login')}}">Login <i class="fas fa-arrow-right"></i></a></li>
@else
<li><a class="default-button" href="{{url('/dashboard')}}">Dashboard <i class="fas fa-arrow-right"></i></a></li>
@endguest
</ul>
</div>
</div>
</div>

<div class="main-nav">
<div class="container">
<nav class="navbar navbar-expand-md navbar-light">
<a class="navbar-brand" href="{{url('/')}}">
<img src="/landing/assets/images/logo.png" height="150" width="200" alt="logo" />
</a>
<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item"><a href="{{url('/dashboard')}}" class="nav-link">Dashbord</a></li>


@guest
<li class="nav-item"><a href="{{url('/login')}}" class="nav-link">Login</a></li>
@else
<li class="nav-item"><a href="{{url('/dashboard')}}" class="nav-link">Dashboard</a></li>
@endguest

</ul>

</ul>
<div class="menu-sidebar">
    @guest
<a class="default-button" href="{{url('login')}}">Login<i class="fas fa-arrow-right"></i>
    @else
    <a class="default-button" href="{{url('/dashboard')}}">Dashboard<i class="fas fa-arrow-right"></i>
@endguest
</a>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>


<section class="home-banner">
<div class="container">
<div class="row align-items-center">
<div class="col-lg-6">
<div class="banner-text-area banner-text-area-2">
<h6>Quick Cash Tokens</h6>
<h1>Buy and sell cash tokens quickly</h1>
<p>Get up and take charge of your future with quick cash tokens</p>
<div class="banner-buttons">
<ul>
    @guest
<li><a class="btn-danger" href="{{url('register')}}">Join Now <i class="fas fa-arrow-right"></i></a></li>
    @else
<li><a class="default-button" href="{{url('dashboard')}}">Dashboard <i class="fas fa-arrow-right"></i></a></li>
    @endguest
</ul>
</div>
</div>
</div>
<div class="col-lg-6">
<div class="banner-img-2">
<img src="/landing/assets/images/banner.png" alt="image">
</div>
</div>
</div>
</div>
</section>






















<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="/landing/assets/js/jquery.min.js"></script>
<script src="/landing/assets/js/jquery-ui.min.js"></script>
<script src="/landing/assets/js/bootstrap.bundle.min.js"></script>
<script src="/landing/assets/js/meanmenu.js"></script>
<script src="/landing/assets/js/owl.carousel.min.js"></script>
<script src="/landing/assets/js/magnific-popup.min.js"></script>
<script src="/landing/assets/js/TweenMax.js"></script>
<script src="/landing/assets/js/nice-select.min.js"></script>
<script src="/landing/assets/js/form-validator.min.js"></script>
<script src="/landing/assets/js/contact-form-script.js"></script>
<script src="/landing/assets/js/ajaxchimp.min.js"></script>
<script src="/landing/assets/js/countdown.min.js"></script>
<script src="/landing/assets/js/appear.min.js"></script>
<script src="/landing/assets/js/barfiller.js"></script>
<script src="/landing/assets/js/odometer.min.js"></script>
<script src="/landing/assets/js/custom.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/617b3c79f7c0440a59207ebd/1fj4m8fms';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>


</html>