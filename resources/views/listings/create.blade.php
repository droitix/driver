@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="buysell wide-xs m-auto">
                                <div class="buysell-nav text-center">
                                    <ul class="nk-nav nav nav-tabs nav-tabs-s2">
                                        
                                       
                                    </ul>
                                </div><!-- .buysell-nav -->
                                <div class="buysell-title text-center">
                                    <h2 class="title">Buy Tokens</h2>
                                </div><!-- .buysell-title -->
                                <div class="buysell-block">
                                    <form action="{{ route('listings.store', [$area]) }}" method="post" class="buysell-form">

                                        <div class="buysell-field form-group">
                                            <div class="form-label-group">
                                                <label class="form-label" for="buysell-amount"></label>
                                            </div>
                                            <label for="cars">Select Package</label>
                                             <div class="form-control-group">
<select name="value" id="value" form="carform">
  <option value="3">50% in 5 Days</option>
  <option value="10">120% in 10 Days</option>
  <option value="15">170% in 15 Days</option>
  
</select>
</div>
                                            <div class="form-control-group">
                                              
                                                <input type="text" class="form-control form-control-lg form-control-number" id="buysell-amount" name="amount" placeholder="Amount">
                                                
                                            </div>
                                            <div class="form-note-group">
                                                <span class="buysell-min form-note-alt">Minimum: R500  </span>
                                               
                                            </div>
                                        </div><!-- .buysell-field -->
                                        
                                        <input type="hidden" class="form-control" name="value" id="value" value="0.10">
                                       <input type="hidden" class="form-control" name="period" id="period" value="28">
                                        <input type="hidden" class="form-control" name="current" id="current" value="0">
                                          <input type="hidden" class="form-control" name="recommit" id="period" value="0">
                                   <input type="hidden" class="form-control" name="type" id="type" value="1">
                                           <input type="hidden" class="form-control" name="percent" id="percent" value="1.35">
                                         <input type="hidden" class="form-control" name="area_id" id="area" value="5">
                                         <input type="hidden" class="form-control" name="category_id" id="area" value="2">
                                        <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-warning" data-toggle="modal" data-target="#buy-coin">Create Deal</button>
                                        </div><!-- .buysell-field -->
                                        <div class="form-note text-base text-center">R500 minimum<a href="#"></a>.</div>
                                           {{ csrf_field() }}

                                    </form><!-- .buysell-form -->
                                </div><!-- .buysell-block -->
                            </div><!-- .buysell -->
                        </div>
                    </div>
                </div>
                <!-- content @e -->


@endsection
