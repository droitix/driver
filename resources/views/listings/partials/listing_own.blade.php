@if(!$listing->type())
<!-- content @s -->       
<div class="nk-block">
<div class="row g-gs">
   <div class="col-md-6">
      <div class="card card-bordered pricing">
         <div class="pricing-head">
            <div class="pricing-title">
               <h4 class="card-title title">Contract Expired</h4>
               <p class="sub-text">This investment was stopped</p>
            </div>
            <div class="card-text">
               <div class="row">
                  <div class="col-6">
                     <span class="h4 fw-500"></span>
                     <span class="sub-text">Daily Interest</span>
                  </div>
                  <div class="col-6">
                     <span class="h4 fw-500"></span>
                     <span class="sub-text">Contract Balance</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="pricing-body">
            <ul class="pricing-features">
            </ul>
            @if (session()->has('impersonate'))
            <div style="margin-top: 20px">
               <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
               </form>
               <li>
                  <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div>
               </li>
               @endif
            </div>
         </div>
      </div>
      <!-- .col -->
   </div>
   <!-- .nk-block -->
</div>
@else
@if($listing->recommit())
<div class="col-md-6">
   <div class="card card-bordered pricing recommend text-center">
      <span class="pricing-badge badge badge-primary">Bonus</span>
      <div class="pricing-body">
         <div class="pricing-media">
            <img src="./images/icons/plan-s3.svg" alt="">
         </div>
         <div class="pricing-title w-220px mx-auto">
            <h5 class="title">Bonus Withdrawal</h5>
            <span class="sub-text">Withdrawal Request has been sent </span>
         </div>
         <h4 style="color:red">{{$listing->bitcoin}}</h4>
         <div class="pricing-amount">
            <div class="amount">{{ Auth::user()->area->unit }} {{$listing->amount}}<span></span></div>
            <span class="bill">It takes 12 Hours or Less to recieve your money</span><br><br>
            <span class="bill">Send Whatsapp to {{ Auth::user()->area->skrill }} For fast Approval </span>
         </div>
      </div>
      @if (session()->has('impersonate'))
      <h5>{{ Auth::user()->name }} {{ Auth::user()->surname }} {{ Auth::user()->phone_number }} {{ Auth::user()->account }} {{ Auth::user()->area->unit }} {{$listing->amount}} BONUS WITHDRAWAL </h5>
      <div style="margin-top: 20px">
         <a href="{{ route('listings.edit', [$area, $listing]) }}" class="">Approve Bonus Withdrawal</a>
      </div>
      <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
         {{ csrf_field() }}
         {{ method_field('DELETE') }}
      </form>
      <li><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></li>
      @else
      @endif
   </div>
</div>
<br><br>
@else
@if(!$listing->matched())
<div class="col-md-6">
   <div class="card card-bordered pricing recommend text-center">
      <span class="pricing-badge badge badge-danger">Not Paid</span>
      <div class="pricing-body">
         <div class="pricing-media">
            <img src="/landing/assets/images/logo.png" alt="">
         </div>
         <div class="pricing-title w-220px mx-auto">
            <h5 class="title">Quick tokens</h5>
            <span class="sub-text">Not Paid Yet</span>
         </div>
         <div class="pricing-amount">
            <div class="amount">R {{$listing->amount}}<span></span></div>
            <span class="bill"></span>
         </div>
         <div class="pricing-action">
            <a href="{{url('pay')}}" class="btn btn-warning">Check Details to Pay</a>
         </div>
      </div>
      @if (session()->has('impersonate'))
      <div style="margin-top: 20px">
         <div class="pricing-action">
            <a href="{{ route('listings.edit', [$area, $listing]) }}" class="btn btn-primary">Admin Confirm Payment</a>
         </div>
      </div>
      <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
         {{ csrf_field() }}
         {{ method_field('DELETE') }}
      </form>
      <li>
         <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div>
      </li>
      @endif
   </div>
   <!-- .pricing -->
</div>
<!-- .col --><br>
@else
<!-- content @s -->       
<div class="nk-block">
<div class="row g-gs">
   <div class="col-md-6">
      <div class="card card-bordered pricing">
         <div class="pricing-head">
            <div class="pricing-title">
               @php($maturitydate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*24)))
               @php($withdrawaldate = \Carbon\Carbon::parse($listing->updated_at->addHours($listing->days*2)))
               @if ($maturitydate->isPast())
               <h4 class="card-title title">Contract Expired</h4>
               @else
               <h4 class="card-title title">Maturing Token</h4>
               @endif
               <p class="sub-text"></p>
               <p class="sub-text">Bought On: {{$listing->updated_at}}</p>
            </div>
            @php ($sum = 0)
            @foreach($listing->comments as $comment)
            @php ($sum += $comment->split)
            @if ($loop->last)
            @endif
            @endforeach
            @php ($now = \Carbon\Carbon::now())
            @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
            @php($percentage = $listing->value)
            @php($multiplier = ($listing->amount-$listing->current))
            <div class="card-text">
               <div class="row">
                  <div class="col-6">
                    
                  </div>
                  <div class="col-6">
                     <span class="h4 fw-500">R{{($listing->amount-$sum)+(($multiplier)*($percentage)*$days)-($listing->amount)-($listing->scrap)}}</span>
                     <span class="sub-text">Available to Withdraw</span>
                  </div>
               </div>
            </div>
         </div>
         <div class="pricing-body">
            <ul class="pricing-features">
               <li><span class="w-50">Token</span> - <span class="ml-auto">R {{$listing->amount}}</span></li>
            </ul>
            @if($listing->comments->count())
            @foreach($listing->comments as $comment)
            @if (!$comment->approvals->count())
            <div class="pricing-action">
               <button  class="btn btn-danger">R {{$comment->split}} Withdrawal Pending (Expect within 5 Hours to recieve)</button>
            </div>
            @else
            @endif
            @if (session()->has('impersonate'))
            @if ($comment->approvals->count())
            @else
            <form action="{{ route('approvals.store', [$comment->id]) }}" method="post">
               <input type="hidden" class="form-control" name="body" id="body" value="Approved">
               <div class="pricing-action">
                  <h5>{{ Auth::user()->name }} {{ Auth::user()->surname }} {{ Auth::user()->phone_number }} {{ Auth::user()->account }} {{ Auth::user()->area->unit }} {{$comment->split}} </h5>
                  <button type="submit" class="btn btn-primary">{{ Auth::user()->area->unit }} {{$comment->split}} Approve Withdrawal</button>
               </div>
               {{ csrf_field() }}
            </form>
            @endif
            @endif 
            @endforeach  
            @endif
            <div class="pricing-action">
               <a href="{{ route('listings.reinvest', [$area, $listing]) }}" class="btn btn-success">Re-Invest Token </a>
            </div>
            <div class="pricing-action">
               <a href="{{ route('listings.apply', [$area, $listing]) }}" class="btn btn-outline-light">Sell Now</a>
            </div>
            @if (session()->has('impersonate'))
            <div class="pricing-action">
               <a href="{{ route('listings.extend', [$area, $listing]) }}" class="btn btn-danger">Admin Edit Listing</a>
            </div>
            @endif
            @if (session()->has('impersonate'))
            <div style="margin-top: 20px">
               <form action="{{ route('listings.destroy', [$area, $listing]) }}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                  {{ csrf_field() }}
                  {{ method_field('DELETE') }}
               </form>
               <li>
                  <div class="pricing-action"><a href="#" class="btn_1 gray delete" class="btn_1 gray delete"  onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"><i class="fas fa-times-circle"></i> Admin Delete this request</a></div>
               </li>
               @endif
            </div>
         </div>
      </div>
      <!-- .col -->
   </div>
   <!-- .nk-block -->
</div>
<br><br>
@endif                          
@endif
@endif