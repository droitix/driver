<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from templates.hibootstrap.com/gliter/default/profile-authentication.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Oct 2021 01:31:08 GMT -->
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="/landing/assets/css/animate.min.css">
<link rel="stylesheet" href="/landing/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/landing/assets/css/fontawsome.min.css">
<link rel="stylesheet" href="/landing/assets/fonts/font/flaticon.css">
<link rel="stylesheet" href="/landing/assets/css/meanmenu.min.css">
<link rel="stylesheet" href="/landing/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/landing/assets/css/nice-select.min.css">
<link rel="stylesheet" href="/landing/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/landing/assets/css/magnific-popup.min.css">
<link rel="stylesheet" href="/landing/assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="/landing/assets/css/odometer.min.css">
<link rel="stylesheet" href="/landing/assets/css/barfiller.css">
<link rel="stylesheet" href="/landing/assets/css/style.css">
<link rel="stylesheet" href="/landing/assets/css/responsive.css">
<title>Driver Finance | Account</title>
<link rel="icon" type="image/png" href="assets/images/fav-icon.png">
</head>
<body>

<div class="header-area header-area-2">
<div class="navbar-area">

<div class="main-responsive-nav">
<div class="container">
<div class="mobile-nav">
<a href="{{url('/')}}" class="logo"><img src="/landing/assets/images/phone1.png" alt="logo" /></a>
<ul class="menu-sidebar menu-small-device">
<li><a class="default-button" href="{{url('register')}}">Investment Rules <i class="fas fa-arrow-right"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="main-nav">
<div class="container">
<nav class="navbar navbar-expand-md navbar-light">
<a class="navbar-brand" href="{{url('/')}}">
<img src="/landing/assets/images/logo.png" height="150" width="200" alt="logo" />
</a>
<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item"><a href="{{url('/')}}" class="nav-link">Home</a></li>
<li class="nav-item"><a href="{{url('/terms')}}" class="nav-link">How to invest</a></li>
<li class="nav-item"><a href="{{url('/faq')}}" class="nav-link">FAQ</a></li>
<li class="nav-item"><a href="{{url('/contact')}}" class="nav-link">Contact Us</a></li>
</ul>
<div class="menu-sidebar">
<a class="default-button" href="{{url('register')}}">Create Account<i class="fas fa-arrow-right"></i></a>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>





<section class="uni-banner">
<div class="container">
<div class="uni-banner-text-area">
<h1>How to Invest</h1>
<ul>
 <li><a href="{{url('/')}}">Home</a></li>
<li>How to</li>
</ul>
</div>
</div>
</section>


<section class="pricing pt-70 pb-100">
<div class="container">
<div class="row">


<div class="col-lg-4 col-md-6 col-sm-12 col-12">
<div class="pricing-card p-active">
<span> K300</span>
<h5>Start From</h5>
<h2>K300 <sub>as the Minimum</sub></h2>
<p>You withdraw K50 Daily and can withdraw your capital after 3 days</p>
<p>This means you get 10% of whatever amount you invest everyday</p>
<ul>
<li><i class="far fa-check-circle"></i> You can withdraw daily</li>
<li><i class="far fa-check-circle"></i> You can leave your money to grow in your account and withdraw once</li>
<li><i class="far fa-check-circle"></i> You can request a withdrawal of all your investment</li>
<li><i class="far fa-check-circle"></i> 24/7 Support</li>
</ul>
<a class="default-button default-button-2" href="{{ route('listings.create', [$area]) }}">Deposit Now <i class="fas fa-arrow-right"></i></a>
</div>
</div>

</div>
</div>
</section>


<div class="copyright bg-071327">
<div class="container">
<p>Copyright &copy; 2021. <strong>Driver Finance</strong> All Rights Reserved <a target="_blank" href="#"></a></p>
</div>
</div>


<div class="go-top go-top-3"><i class="fas fa-chevron-up"></i></div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>
<script src="/landing/assets/js/jquery-ui.min.js"></script>
<script src="/landing/assets/js/bootstrap.bundle.min.js"></script>
<script src="/landing/assets/js/meanmenu.js"></script>
<script src="/landing/assets/js/owl.carousel.min.js"></script>
<script src="/landing/assets/js/magnific-popup.min.js"></script>
<script src="/landing/assets/js/TweenMax.js"></script>
<script src="/landing/assets/js/nice-select.min.js"></script>
<script src="/landing/assets/js/form-validator.min.js"></script>
<script src="/landing/assets/js/contact-form-script.js"></script>
<script src="/landing/assets/js/ajaxchimp.min.js"></script>
<script src="/landing/assets/js/countdown.min.js"></script>
<script src="/landing/assets/js/appear.min.js"></script>
<script src="/landing/assets/js/barfiller.js"></script>
<script src="/landing/assets/js/odometer.min.js"></script>
<script src="/landing/assets/js/custom.js"></script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/617b3c79f7c0440a59207ebd/1fj4m8fms';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>

<!-- Mirrored from templates.hibootstrap.com/gliter/default/profile-authentication.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Oct 2021 01:31:08 GMT -->
</html>