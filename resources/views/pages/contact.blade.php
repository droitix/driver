<!DOCTYPE html>
<html lang="zxx">

<!-- Mirrored from templates.hibootstrap.com/gliter/default/profile-authentication.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Oct 2021 01:31:08 GMT -->
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">


<link rel="stylesheet" href="/landing/assets/css/animate.min.css">
<link rel="stylesheet" href="/landing/assets/css/bootstrap.min.css">
<link rel="stylesheet" href="/landing/assets/css/fontawsome.min.css">
<link rel="stylesheet" href="/landing/assets/fonts/font/flaticon.css">
<link rel="stylesheet" href="/landing/assets/css/meanmenu.min.css">
<link rel="stylesheet" href="/landing/assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="/landing/assets/css/nice-select.min.css">
<link rel="stylesheet" href="/landing/assets/css/owl.theme.default.min.css">
<link rel="stylesheet" href="/landing/assets/css/magnific-popup.min.css">
<link rel="stylesheet" href="/landing/assets/css/jquery-ui.min.css">
<link rel="stylesheet" href="/landing/assets/css/odometer.min.css">
<link rel="stylesheet" href="/landing/assets/css/barfiller.css">
<link rel="stylesheet" href="/landing/assets/css/style.css">
<link rel="stylesheet" href="/landing/assets/css/responsive.css">
<title>Driver Finance | Account</title>
<link rel="icon" type="image/png" href="assets/images/fav-icon.png">
</head>
<body>

<div class="header-area header-area-2">
<div class="navbar-area">

<div class="main-responsive-nav">
<div class="container">
<div class="mobile-nav">
<a href="{{url('/')}}" class="logo"><img src="/landing/assets/images/phone1.png" alt="logo" /></a>
<ul class="menu-sidebar menu-small-device">
<li><a class="default-button" href="{{url('register')}}">Investment Rules <i class="fas fa-arrow-right"></i></a></li>
</ul>
</div>
</div>
</div>

<div class="main-nav">
<div class="container">
<nav class="navbar navbar-expand-md navbar-light">
<a class="navbar-brand" href="{{url('/')}}">
<img src="/landing/assets/images/logo.png" height="150" width="200" alt="logo" />
</a>
<div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
<ul class="navbar-nav">
<li class="nav-item"><a href="{{url('/')}}" class="nav-link">Home</a></li>
<li class="nav-item"><a href="{{url('/terms')}}" class="nav-link">How to invest</a></li>

<li class="nav-item"><a href="{{url('/contact')}}" class="nav-link">Contact Us</a></li>
</ul>
<div class="menu-sidebar">
<a class="default-button" href="{{url('register')}}">Create Account<i class="fas fa-arrow-right"></i></a>
</div>
</div>
</nav>
</div>
</div>
</div>
</div>




<section class="uni-banner">
<div class="container">
<div class="uni-banner-text-area">
<h1>Contact Us</h1>
<ul>
<li><a href="index.html">Home</a></li>
<li>Contact</li>
</ul>
</div>
</div>
</section>


<section class="contact ptb-100">
<div class="container">
<div class="row">
<div class="col-lg-4">
<div class="contact-card-area pr-20">
<div class="default-section-title">
<h3>Contact Us</h3>
<p>Take advantage of our live chat and talk to us.</p>
</div>

<div class="contact-card">
<h4><i class="fas fa-envelope"></i> Email: </h4>
<p><a href="/cdn-cgi/l/email-protection#e9818c858586a98e85809d8c9bc78a8684"><span class="__cf_email__" data-cfemail="4c24292020230c2b202538293e622f2321">admin@driverfinance.co.za</span></a></p>
</div>
<div class="contact-card">
<h4><i class="fas fa-phone"></i> Call: </h4>
<p><a href="tel:+029222110">+26071894599</a></p>
</div>

</div>
</div>
<div class="col-lg-8">
<div class="contact-page-form-area pt-30">
<form id="contactForm">
<div class="row">
<div class="col-md-6 col-sm-6 col-12">
<div class="form-group">
<input type="text" class="form-control" placeholder="Name*" id="name" required data-error="Please enter your name">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-6 col-sm-6 col-12">
<div class="form-group">
<input type="email" name="email" class="form-control" placeholder="Email*" id="email" required data-error="Please enter your Email">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-6 col-sm-6 col-12">
<div class="form-group">
<input type="text" name="phone_number" class="form-control" placeholder="Phone Number*" id="phone_number" required data-error="Please enter your phone number">
<div class="help-block with-errors"></div>
 </div>
</div>
<div class="col-md-6 col-sm-6 col-12">
<div class="form-group">
<input type="text" name="msg_subject" class="form-control" placeholder="Subject-" id="msg_subject" required data-error="Please enter your subject">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-6 col-sm-6 col-12">
<div class="form-group">
<input type="text" name="company_name" class="form-control" placeholder="Company Name*" id="company_name" required data-error="Please enter your Company Name">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-6 col-sm-6 col-12">
<div class="form-group">
<input type="text" name="website" class="form-control" placeholder="Website Link*" id="website" required data-error="Please enter your Website Link">
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-12 col-sm-12 col-12">
<div class="form-group">
<textarea name="message" id="message" class="form-control" placeholder="Your Messages.." cols="30" rows="5" required data-error="Please enter your message"></textarea>
<div class="help-block with-errors"></div>
</div>
</div>
<div class="col-md-12 col-sm-12 col-12">
<button class="default-button default-button-3" type="submit"><span>Send Message <i class="fas fa-arrow-right"></i></span></button>
<div id="msgSubmit" class="h6 text-center hidden"></div>
<div class="clearfix"></div>
</div>
</div>
</form>
</div>
</div>
</div>
</div>
</section>



<div class="copyright bg-071327">
<div class="container">
<p>Copyright &copy; 2021. <strong>Driver Finance</strong> All Rights Reserved <a target="_blank" href="#"></a></p>
</div>
</div>


<div class="go-top go-top-3"><i class="fas fa-chevron-up"></i></div>

<script data-cfasync="false" src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery.min.js"></script>
<script src="/landing/assets/js/jquery-ui.min.js"></script>
<script src="/landing/assets/js/bootstrap.bundle.min.js"></script>
<script src="/landing/assets/js/meanmenu.js"></script>
<script src="/landing/assets/js/owl.carousel.min.js"></script>
<script src="/landing/assets/js/magnific-popup.min.js"></script>
<script src="/landing/assets/js/TweenMax.js"></script>
<script src="/landing/assets/js/nice-select.min.js"></script>
<script src="/landing/assets/js/form-validator.min.js"></script>
<script src="/landing/assets/js/contact-form-script.js"></script>
<script src="/landing/assets/js/ajaxchimp.min.js"></script>
<script src="/landing/assets/js/countdown.min.js"></script>
<script src="/landing/assets/js/appear.min.js"></script>
<script src="/landing/assets/js/barfiller.js"></script>
<script src="/landing/assets/js/odometer.min.js"></script>
<script src="/landing/assets/js/custom.js"></script>
</body>

<!-- Mirrored from templates.hibootstrap.com/gliter/default/profile-authentication.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 20 Oct 2021 01:31:08 GMT -->
</html>