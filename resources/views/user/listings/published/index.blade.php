@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')




      <div class="nk-content ">
                    <div class="container wide-xl">
                        <div class="nk-content-inner">
                  
                            <div class="nk-content-body">
                                <div class="nk-content-wrap">
                                    <div class="nk-block-head nk-block-head-sm">
                                        <div class="nk-block-between g-3">
                                            <div class="nk-block-head-content">
                                                <h3 class="nk-block-title page-title">Tokens Panel</h3>
                                                <div class="nk-block-des text-soft">
                                                    <p>Manage Tokens.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div><!-- .nk-block-head -->

                    
                     
                    
                            
                          
                              @if ($listings->count())
        @each ('listings.partials.listing_own', $listings, 'listing')
        {{ $listings->links() }}
    @else
       <th>  <p style="text-align: center;">YOU HAVE NOT BOUGHT ANY TOKENS YET</p></th>
    @endif

 

                            </div>
                           
                        </div>
                            
                        </div>
                  


                   
                </div>
            </div>
            <!-- account end -->

@endsection
