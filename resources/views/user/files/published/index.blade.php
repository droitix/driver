@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="buysell wide-xs m-auto">
                                <div class="buysell-nav text-center">
                                    <ul class="nk-nav nav nav-tabs nav-tabs-s2">
                                        <li class="nav-item">
                                            <a class="nav-link" href="{{ route('listings.create', [$area]) }}">NRC VERIFICATION</a>
                                        </li>
                                       
                                    </ul>
                                </div><!-- .buysell-nav -->
                                <div class="buysell-title text-center">
                                    <h4 class="title">YOUR NRC WILL BE VERIFIED HERE FOR INSTANT WITHDRAWALS.IT TAKES ATLEAST 24HRS AND IF MORE CONTACT +260779910420</h4>
                                </div><!-- .buysell-title -->
                                <div class="buysell-block">
                                    <table class="table mb-0 table-hover">
                                           <tbody>
                                     @if ($files->count())
        @each ('resumes.partials._file_own', $files, 'file')

    @else
        <tr class="mb30">
                                                <th scope="row">
                                                    <ul>
                                                        <li class="list-inline-item"><a href=""><i class="fe fe-document"></i></a></li>
                                                        <li class="list-inline-item cv_sbtitle">UPLOAD NRC NOW<u> <li><br><a href="{{ url('uploaddocs') }}" class="btn btn-lg btn-warning"><span>Upload NRC for Phone Verification</span></a></li></u> </li>
                                                    </ul>
                                                </th>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td>

                                                </td>
                                            </tr>

    @endif

                                        </tbody>
                                        </table>
                                </div><!-- .buysell-block -->
                            </div><!-- .buysell -->
                        </div>
                    </div>
                </div>
                <!-- content @e -->


@endsection
