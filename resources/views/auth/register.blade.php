@extends('layouts.regapp')
@section('title', 'register')

@section('description')



@endsection
@section('content')
   
<div class="nk-block nk-block-middle nk-auth-body wide-xs">
                        <div class="brand-logo pb-4 text-center">
                            <a href="{{url('/')}}" class="logo-link">
                                <img class="logo-light logo-img logo-img-lg" src="/landing/assets/images/logo.png" srcset="/landing/assets/images/logo.png 2x" alt="logo">
                                <img class="logo-dark logo-img logo-img-lg" src="/landing/assets/images/logo.png" srcset="/landing/assets/images/logo.png 2x" alt="logo-dark">
                            </a>
                        </div>
                        <div class="card card-bordered">
                            <div class="card-inner card-inner-lg">
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        
                                        <div class="nk-block-des">
                                            <p>Create  Account </p>
                                        </div>
                                    </div>
                                </div>
                                   <form action="{{ route('register') }}" method="POST" autocomplete="off" id="register_user" >
                                    @csrf
                                    <div class="form-group">
                                        <label class="form-label" for="name">First Name</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} form-control-lg" name="name" type="text" value="{{ old('name') }}" required autofocus  placeholder="Enter First Name" >
                                        </div>
                                         @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                                    </div>

                                    <div class="form-group">
                                        <label class="form-label" for="name">Last Name</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }} form-control-lg" name="surname" type="text" value="{{ old('surname') }}" required autofocus  placeholder="Enter Last Name" >
                                        </div>
                                         @if ($errors->has('surname'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                                    </div>

                                                      

                                    <div class="form-group">
                                        <label class="form-label" for="name">Phone Number</label>
                                        <div class="form-control-wrap">
                                            <input type="text" class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }} form-control-lg" name="phone_number" type="text" value="{{ old('phone_number') }}" required autofocus  placeholder="Enter Phone Number" >
                                        </div>
                                        @if ($errors->has('phone_number'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone_number') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="email">Email</label>
                                        <div class="form-control-wrap">
                                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg" name="email" value="{{ old('email') }}" required   id="email" placeholder="Enter your email address">
                                        </div>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="password">Password</label>
                                        <div class="form-control-wrap">
                                            <a href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg " name="password" required placeholder="Choose a password" id="password" placeholder="Enter your password">
                                        </div>
                                        @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label" for="password">Type Password Again</label>
                                        <div class="form-control-wrap">
                                            <a href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                             <input id="password-confirm" type="password" class="form-control form-control-lg" name="password_confirmation" required placeholder="Type password again">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="custom-control custom-control-xs custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="checkbox">
                                            <label class="custom-control-label" for="checkbox">I agree to <a href="#">Terms and Conditions</a></label>
                                        </div>
                                    </div>
                                    <input type="hidden" name="area_id" value="3">
                                    <div class="form-group">
                                        <button class="btn btn-lg btn-danger btn-block">Register</button>
                                    </div>
                                </form>
                                <div class="form-note-s2 text-center pt-4"> Already have an account? <a href="{{url('login')}}"><strong>Sign in instead</strong></a>
                                </div>
                                
                            </div>
                        </div>
                    </div>
       
        
        

@endsection
