 
@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')



 <!-- content @s -->
                <div class="nk-content nk-content-fluid">
                    <div class="container-xl wide-lg">
                        <div class="nk-content-body">
                            <div class="nk-block-head">
                                <div class="nk-block-head-sub"><span>
                          {{$referralink}}
                                </div>
                                <div class="nk-block-between-md g-4">
                                    <div class="nk-block-head-content">
                                         <div class="buysell-field form-action">
                                           
                                        </div>
                                         <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-success" data-toggle="modal" data-target="#buy-coin"><h2 class="nk-block-title fw-normal">{{Auth::user()->name}} {{Auth::user()->surname}}</h2></button>
                                        </div>

                   @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                 @if($listing->type())
                            @if($listing->matched())
                               @php ($now = \Carbon\Carbon::now())
                                 @php($days = \Carbon\Carbon::parse($listing->updated_at)->diffInDays($now))
                                       @php($percentage = $listing->value)

                                        @php($multiplier = ($listing->amount-$listing->current))
 
                             
                            @php ($sum += (($multiplier)*($percentage*$days))-$listing->scrap)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                         @endif
                     @endforeach

                     @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                       @if($listing->type())
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
                         @endif
                      @endforeach
                    

                        @php ($balance = $sum-$diff)
                                         <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-success" data-toggle="modal" data-target="#buy-coin"><h2 class="nk-block-title fw-normal">Profits: R <?php echo number_format((float)$balance, 2, '.', ''); ?> </h2></button>
                                        </div>
                                                                                  @php ($sum = 0)

                 @foreach(Auth::user()->listings as $listing)
                   
                            @if($listing->matched())


                            @php ($sum += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif


                     @endforeach
                     <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-warning" data-toggle="modal" data-target="#buy-coin"><h2 class="nk-block-title fw-normal">Tokens Amount: R {{$sum}}.00</h2></button>
                                        </div>
                           @php ($diff = 0)

                       @foreach(Auth::user()->listings as $listing)
                      
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
               
                      @endforeach
                             <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-danger" data-toggle="modal" data-target="#buy-coin"><h2 class="nk-block-title fw-normal">Withdrawals: R {{$diff}}.00</h2></button>
                                        </div>
                                                       @php ($withdrawn = 0)

                 @foreach(Auth::user()->listings as $listing)
                            @if($listing->recommit())


                            @php ($withdrawn += $listing->amount)

                           @if ($loop->last)

                           @endif

                           @else

                         @endif
                     @endforeach
                                    
                                         <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-primary" data-toggle="modal" data-target="#buy-coin"><h2 class="nk-block-title fw-normal">Bonus: R {{\openjobs\Bonus::where('referer_id', auth()->user()->id)->sum('bonus_amount')-($withdrawn)}}.00</h2></button>
                                        </div>


                                         <div class="buysell-field form-action">
                                            <button type="submit" class="btn btn-lg btn-block btn-secondary" data-toggle="modal" data-target="#buy-coin"><h2 class="nk-block-title fw-normal">Downliners: {{ Auth::user()->referrals()->count()}}</h2></button>
                                        </div>
                                        
                                                                               

                                        
                                    </div><!-- .nk-block-head-content -->
                                  
                                </div><!-- .nk-block-between -->
                            </div><!-- .nk-block-head -->
                            

                       @foreach(Auth::user()->listings as $listing)
                      
                           @if($listing->matched())

                            @foreach($listing->comments as $comment)


                            @php ($diff += $comment->split)



                            @endforeach
                            @else

                         @endif
               
                      @endforeach
                                                                   
                                                                </div>
                                                            </div>
                                                            <div class="nk-wg7-foot">
                                                                
                                                            </div>
                                                          
                                                        </div><!-- .nk-wg7 -->
                                                    </div><!-- .card-inner -->
                                                </div><!-- .card -->
                                            </div><!-- .nk-block -->
                                        </div><!-- .nk-block -->
                                    </div><!-- .col -->
                         

                                                               
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div><!-- .col -->
                                                
                                            </div><!-- .row -->
                                        </div><!-- .nk-block -->
                                       
                                    </div><!-- .col -->
                                </div><!-- .row -->
                            </div><!-- .nk-block -->
                            
                            
                        </div>
                    </div>
                </div>
                <!-- content @e -->

@endsection