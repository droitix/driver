@extends('layouts.app')

@section('title', 'Dashboard')

@section('description')

@endsection

@section('content')

 


             <!-- breadcrumb begin -->
            <div class="breadcrumb-oitila db-breadcrumb">
                <div class="container">
                    <div class="row justify-content-lg-around">
                        <div class="col-xl-6 col-lg-7 col-md-5 col-sm-6 col-8">
                            <div class="part-txt">
                                <h1>UnPaid Investments</h1>
                                <ul>
                                    <li>Home</li>
                                    <li>Investments</li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-6 col-lg-5 col-md-7 col-sm-6 col-4 d-flex align-items-center">
                            <div class="db-user-profile">
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- breadcrumb end -->

            <!-- account begin -->
            <div class="user-dashboard">
                <div class="container">

                    

                                      <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="transactions-table">
                                <h3 class="title">
          
                                </h3>
                                <div class="table-responsive">
                                            <table class="table table-centered table-nowrap">
                                                <thead>
                                                    <tr>

                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Email</th>
                                                        <th>Contact</th>
                                                        <th>Amount</th>
                                                        <th>Bank</th>
                                                        
                                                        <th>Creation Date</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                      @foreach ($listings as $listing)
                                                   @if (!$listing->matched())



                                        <tr>
                                           
                                             
                                             <td style="background-color: yellow">{{$listing->id}} DEPOSIT

                                            </td>
                                          
                                            <td style="background-color: yellow">{{$listing->user->name}} {{$listing->user->surname}}

                                            </td>
                                            <td style="background-color: yellow">{{$listing->user->email}}</td>
                                            <td style="background-color: yellow">{{$listing->user->phone_number}}</td>
                                            <td style="background-color: yellow">{{$listing->amount}}</td>
                                            <td style="background-color: yellow">{{$listing->user->bank}}  {{$listing->user->account}}</td>
                                            
                                            <td style="background-color: yellow">{{$listing->created_at}}</td>


                                            <td style="background-color: yellow"> <a href="#" class="btn btn-sm bg-danger-light"
                                        onclick="event.preventDefault(); document.getElementById('listings-destroy-form-{{ $listing->id }}').submit();"
                            data-toggle="tooltip" data-placement="bottom" title="Delete Listing"><i class="fe fe-trash"></i>Delete</a></li>

                             <form action="{{route('admin.listing.destroy', [$listing->id])}}" method="post" id="listings-destroy-form-{{ $listing->id }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}
                                    </form></td>
                                

                                    @else



                
                                   @endif

                                        @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                            </div>
                        </div>
                    </div>     
                        
        


                   
                </div>
            </div>
            <!-- account end -->

@endsection
